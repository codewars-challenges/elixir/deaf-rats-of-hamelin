defmodule Kata do
  def count_deaf_rats(s) do
    [left, right] = String.split(s, "P")
    left = count_deaf_rats(left, "~O")
    right = count_deaf_rats(right, "O~")
    left + right
  end

  defp count_deaf_rats(str, filter), do:
    str
    |> remove_blanks()
    |> chunk()
    |> filter(filter)
    |> length()

  defp remove_blanks(str), do:
    str
    |> String.split("")
    |> Enum.filter(fn s -> s != " " end)
    |> Enum.join()

  defp chunk(str), do: for <<s :: binary - 2 <- str>>, do: s
  defp filter(list, str), do: Enum.filter(list, fn s -> s != str end)
end

count_deaf_rats = &Kata.count_deaf_rats/1

IO.inspect(count_deaf_rats.("~O~O~O~O P"))
IO.inspect(count_deaf_rats.("P O~ O~ ~O O~"))
IO.inspect(count_deaf_rats.("~O~O~O~OP~O~OO~"))